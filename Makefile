NAME	=	miniRT
SRCS	=	src/main.c \
			src/ray/ray.c \
			src/error/error.c \
			src/parsing/parse_camera.c \
			src/parsing/parse_ambiant.c \
			src/parsing/parse_light.c \
			src/parsing/parse_resolution.c \
			src/parsing/parse_sphere.c \
			src/parsing/parse_plane.c \
			src/parsing/parser.c \
			src/parsing/utils_pars.c \
			src/parsing/utils.c \
			src/shape/ray_sphere.c \
			src/shape/ray_plane.c \
			src/utils/tools.c \
			src/utils/destruct/destruct.c \
			src/utils/event/key.c \
			src/utils/event/mouse.c \
			src/utils/matrice/matrix.c \
			src/utils/matrice/manage_matrix.c \
			src/utils/matrice/rotation.c \
			src/utils/matrice/translation.c \
			src/utils/vector/vector.c \
			src/utils/colors.c \
			src/shape/ray_obj.c \
			src/parsing/parse_cylinder.c \
			src/shape/ray_cylinder.c \
			src/utils/window/window.c

OBJS	=	$(SRCS:.c=.o)
LIBFT	=	libft/libft.a
MINILIB =	minilibx-linux/
MINIO	=	$(MINILIB)libmlx_Linux.a
MINIH	=	$(MINILIB)mlx_linux.h
CC		=	clang
CFLAGS	=	-Wall -Wextra -Werror -g3
INCLUDE	=	-I inc/ -I libft/  -I /usr/include/ -I $(MINILIB)
LDFLAGS =	-lXext -lX11 -lm
MLXFLAGS=	${LDFLAGS} #-O3

all :		${NAME}

.PHONY:		all

%.o : 		%.c
			${CC} ${CFLAGS} ${INCLUDE} -c $< -o ${<:.c=.o}


${NAME} :	${OBJS}
			make -C minilibx-linux -f Makefile
			make -C libft -f Makefile
			${CC} ${CFLAGS} ${OBJS} ${LDFLAGS} ${LIBFT} ${MINIO} ${INCLUDE} -o ${NAME}


clean :
			rm -f ${OBJS}
			make fclean -C libft -f Makefile
			make clean -C minilibx-linux -f Makefile
.PHONY: 	clean


fclean :	clean
			rm -rf ${NAME}
			rm -rf libmlx_Linux.a
.PHONY:		fclean


re :		fclean all
.PHONY:		re


bonus :		all
.PHONY: 	bonus
