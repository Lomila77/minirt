/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 10:07:33 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:14:49 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_list	*ft_lstinit(t_list *lst, t_list *newlist, void *(*f)(void *),
							void (*del)(void *))
{
	t_list	*afternew;

	afternew = NULL;
	if (lst == NULL)
		return (NULL);
	if (lst->next != NULL)
		afternew = ft_lstinit(lst->next, newlist, *f, del);
	if (afternew == NULL)
		return (NULL);
	newlist = malloc(sizeof(t_list));
	if (!newlist)
	{
		ft_lstclear(&afternew, del);
		return (newlist);
	}
	newlist->next = afternew;
	newlist->content = (*f)(lst);
	return (newlist);
}

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*newlst;

	newlst = NULL;
	return (ft_lstinit(lst, newlst, f, del));
}
