/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/27 18:42:14 by huthiess          #+#    #+#             */
/*   Updated: 2020/12/09 16:55:10 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <stdlib.h>
#include <unistd.h>

#define CONTINUE GNL_EOF

#ifndef GNL_BUFFER_SIZE
# define GNL_BUFFER_SIZE 1
#endif

#ifndef GNL_FD_MAX
# define GNL_FD_MAX 4096
#endif

#define GNL_EOF 0
#define GNL_ERROR -1

typedef struct s_gnl_buffer
{
	char	content[GNL_BUFFER_SIZE];
	int		start;
	int		size;
}	t_gnl_buffer;

ssize_t	read_ret(int fildes, void *buf, size_t nbyte, ssize_t *ret)
{
	*ret = read(fildes, buf, nbyte);
	return (*ret);
}

static int	create_new_line(char **line, t_gnl_buffer *buf, int line_len,
							int buf_len)
{
	char	*new;

	new = (char *)ft_calloc(line_len + buf_len + 1, sizeof(*new));
	if (new == NULL)
	{
		free(*line);
		return (GNL_ERROR);
	}
	ft_memcpy(new, *line, line_len);
	ft_memcpy(new + line_len, buf->content + buf->start, buf_len);
	new[line_len + buf_len] = '\0';
	free(*line);
	*line = new;
	return (CONTINUE);
}

static int	append_to(char **line, t_gnl_buffer *buf, int *line_len)
{
	int	buf_len;
	int	is_eol;

	buf_len = 0;
	while (buf_len < buf->size && buf->content[buf->start + buf_len] != '\n')
		buf_len++;
	if (create_new_line(line, buf, *line_len, buf_len) == GNL_ERROR)
		return (GNL_ERROR);
	*line_len += buf_len;
	if (buf_len < buf->size)
		is_eol = buf->content[buf->start + buf_len] == '\n';
	else
		is_eol = 0;
	buf->size -= buf_len + is_eol;
	buf->start = 0;
	if (buf->size != 0)
		buf->start = buf->start + buf_len + is_eol;
	return (is_eol);
}

static ssize_t	read_gnl(int fildes, t_gnl_buffer *buf)
{
	return (read_ret(fildes, buf->content, GNL_BUFFER_SIZE,
			(ssize_t *)&buf->size));
}

int	get_next_line(int fd, char **line)
{
	static t_gnl_buffer	buf[GNL_FD_MAX];
	int					result;
	int					line_len;

	if (line == NULL || read(fd, NULL, 0) == GNL_ERROR
		|| GNL_BUFFER_SIZE <= 0 || GNL_FD_MAX <= 0)
		return (GNL_ERROR);
	*line = (char *)ft_calloc(1, sizeof(*line));
	if (line == NULL)
		return (GNL_ERROR);
	(*line)[0] = '\0';
	line_len = 0;
	if (buf[fd].size != 0)
	{
		result = append_to(line, &buf[fd], &line_len);
		if (result != CONTINUE)
			return (result);
	}
	while (read_gnl(fd, &(buf[fd])) > 0)
	{
		result = append_to(line, &buf[fd], &line_len);
		if (result != CONTINUE)
			return (result);
	}
	return (GNL_EOF);
}
