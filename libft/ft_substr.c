/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 08:48:19 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/20 16:32:04 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
/*

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*str;
	char	*src;
	size_t	i;

	str = NULL;
	i = 0;
	src = (char *)s;
	if (start >= ft_strlen(src))
	{
		if (!(str = malloc(sizeof(char))))
			return (NULL);
		str[0] = '\0';
		return (str);
	}
	len = ft_strlen(src) > len ? len : ft_strlen(&src[start]);
	str = malloc(sizeof(char) * (len + 1));
	if (!str)
		return (str);
	str[len] = '\0';
	while (i < len && src[start])
	{
		str[i] = src[start];
		start++;
		i++;
	}
	return (str);
}
*/
