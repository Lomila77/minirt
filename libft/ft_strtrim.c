/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 09:19:31 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/20 21:43:50 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*dest;
	int		i;
	int		len;

	dest = NULL;
	i = 0;
	len = ft_strlen((char *)s1);
	while (i < len && ft_strchr((char *)set, (char)s1[i]) != NULL)
		i++;
	while (len > i && ft_strrchr((char *)set, (char)s1[len]) != NULL)
		len--;
	len -= i - 1;
	dest = ft_substr((char *)s1, i, len);
	if (dest == NULL)
		return (NULL);
	return (dest);
}
