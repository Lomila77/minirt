/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sitoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 10:04:43 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:11:46 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static long long	ft_wneg(long long n, unsigned char *str)
{
	long long	nbr;

	if (n < 0)
	{
		nbr = n * -1;
		str[0] = '-';
	}
	else
		nbr = n;
	return (nbr);
}

static long long	ft_neg(long long n, long long *val)
{
	long long	nbr;

	if (n < 0)
	{
		*val = 1;
		nbr = n * -1;
	}
	else
		nbr = n;
	return (nbr);
}

static int	long_long_min(long long nbr, unsigned char **str)
{
	if (nbr / 10 == -922337203685477580 && nbr % 10 == 8)
	{
		*str = malloc(sizeof(unsigned char) * 21);
		if (!str)
			return (0);
		ft_strlcpy((char *)*str, "-9223372036854775808", 21);
		return (1);
	}
	return (0);
}

unsigned char	*ft_sitoa(long long n)
{
	long long		nbr;
	long long		len;
	unsigned char	*str;

	len = 0;
	if (long_long_min(n, &str))
		return (str);
	nbr = ft_neg(n, &len);
	while (nbr >= 10)
	{
		nbr /= 10;
		len++;
	}
	len++;
	str = malloc(sizeof(unsigned char) * (len + 1));
	nbr = ft_wneg(n, str);
	str[len] = '\0';
	while (nbr >= 10)
	{
		str[--len] = nbr % 10 + 48;
		nbr /= 10;
	}
	str[--len] = nbr + 48;
	return (str);
}
