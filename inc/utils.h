/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/10 15:21:45 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/14 19:38:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include "minirt.h"
# include <stdbool.h>

/* In tools.c: */
bool	control_val(double min, double max, double val);
double	degres_to_radian(double degres);
void	cross(t_point *src1, t_point *src2, t_point *dst);
void	set_type(t_data *data, t_matrix **obj, t_point **ori,
			t_point **dir);

/* In destruct: */
int		free_obj(t_obj *obj);

/* In event: */
void	tr_transform(int keycode, t_matrix *obj, t_transform *transform,
			t_point *p);
void	ro_transform(int keycode, t_matrix *obj, t_transform *transform,
			t_matrix *res);
int		key(int key, t_data *data);
int		mouse(int button, int x, int y, t_data *data);

/* In window: */
void	new_window(t_data *data, t_window *win);
void	my_mlx_pixel_put(t_mlx *mlx, int x, int y, int color);
int		quit(t_data *data);

/* In vector: */
double	norm(t_point *p);
void	normalize_point(t_point *point);
double	dot_product(t_point *a, t_point *b);
void	substract_point(t_point *a, t_point *b, t_point *res);
void	add_point(t_point *a, t_point *b, t_point *dst);

#endif //UTILS_H
