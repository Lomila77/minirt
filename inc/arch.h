/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arch.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 16:00:08 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 11:27:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARCH_H
# define ARCH_H

# include "../libft/libft.h"
# include <stdbool.h>

# define SP 0
# define PL 1
# define CY 2
# define MAX_OBJ 3

# define ROTATE 0
# define TRANSLATE 1

# define MOVE_T 1
# define MOVE_R	10
# define MOVE_D 1
# define MOVE_H 1

typedef struct s_screen
{
	double	width;
	double	height;
}			t_screen;

typedef struct s_window
{
	t_screen	screen;
	char		*name;
	void		*id;
}	t_window;

typedef struct s_point
{
	double	x;
	double	y;
	double	z;
}	t_point;

typedef struct s_c
{
	u_int8_t	b;
	u_int8_t	g;
	u_int8_t	r;
	u_int8_t	_;
}	t_c;

typedef union u_mlx_color
{
	int	mlx_color;
	t_c	c;
}	t_mlx_color;

typedef struct s_color
{
	double	r;
	double	g;
	double	b;
}	t_color;

typedef struct s_light
{
	double	ratio;
	t_color	color;
}	t_light;

typedef struct s_spot
{
	t_point	ori;
	t_light	light;
}	t_spot;

typedef struct s_vec
{
	t_point	ori;
	t_point	dir;
}	t_vec;

typedef struct s_trig
{
	double	s_lmaj;
	t_vec	lmaj;
	double	tca;
	double	thc;
	double	d;
	double	b;
	double	c;
}	t_trig;

typedef struct s_ray
{
	t_vec	axe;
	t_color	color;
}	t_ray;

typedef struct s_pxl
{
	int		x;
	int		y;
	t_point	val;
}	t_pxl;

typedef struct s_matrix
{
	double	c[4][4];
}	t_matrix;

typedef struct s_sphere
{
	t_point		ori;
	double		diameter;
	t_color		color;
	t_matrix	matrix;
}	t_sphere;

typedef struct s_plane
{
	t_vec		axe;
	t_color		color;
	t_matrix	matrix;
}	t_plane;

typedef struct s_cylinder
{
	t_vec		axe;
	double		diameter;
	double		height;
	t_color		color;
	t_matrix	matrix;
	bool		inside;
}	t_cylinder;

typedef struct s_transform
{
	t_matrix	r_x;
	t_matrix	r_y;
	t_matrix	r_z;
	t_matrix	rr_x;
	t_matrix	rr_y;
	t_matrix	rr_z;
	t_point		t_x;
	t_point		t_y;
	t_point		t_z;
	t_point		rt_x;
	t_point		rt_y;
	t_point		rt_z;
}	t_transform;

/* axe: looking axe camera's
 * o_x: Orthonormal x landmarks
 * o_y: Orthonormal y landmarks
 * o_z: Orthonormal z landmarks
 * fov: Field of view */
typedef struct s_cam
{
	t_vec		axe;
	double		fov;
	double		ratio;
	t_matrix	matrix;
}	t_cam;

typedef struct s_data	t_data;
typedef double			(*t_intersec_func)(void *object, t_vec *ray);
typedef void			(*t_colorize_ray_func)(t_data *data, t_ray *ray,
		double t, void *object);

typedef struct s_closest_object
{
	void				*object;
	double				t;
	t_intersec_func		intersec;
	t_colorize_ray_func	colorize_ray;
	short int			type;
}	t_closest_object;

typedef struct s_obj
{
	int					nb;
	t_list				*form;
	void				(*f)(void *);
	t_intersec_func		intersec;
	t_colorize_ray_func	colorize_ray;
}			t_obj;

typedef struct s_img
{
	void	*id;
	char	*addr;
	int		bits_per_pixel;
	int		line_length;
	int		endian;
}	t_img;

typedef struct s_mlx
{
	void	*id;
	t_img	img;
}	t_mlx;

typedef struct s_data
{
	t_window	window;
	t_light		ambient;
	t_spot		spot;
	t_cam		camera;
	t_obj		array_form[MAX_OBJ];
	t_mlx		mlx;
	t_transform	transform;
	void		*obj_to_move;
	short int	type_obj_to_move;
	bool		set_ambient;
	bool		set_camera;
	bool		set_spot;
}	t_data;

#endif //ARCH_H
