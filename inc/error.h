/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 16:26:31 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 17:12:20 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERROR_H
# define ERROR_H

int	e_bad_entry(char *str);
int	e_open_file(char *str);
int	e_bad_read(char *str);
int	e_malloc_fail(char *str);
int	e_bad_arg(char *str);

#endif //ERROR_H
