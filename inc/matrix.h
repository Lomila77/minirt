/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 16:26:31 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 17:12:20 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX_H
# define MATRIX_H

# include "arch.h"
# include <stdlib.h>

void	set_obj_matrix(t_matrix *matrix, t_point *ori, t_point *dir);

void	init_x_rotation(t_matrix *x, double value);
void	init_y_rotation(t_matrix *y, double value);
void	init_z_rotation(t_matrix *z, double value);

void	init_x_translation(t_point *x, double value);
void	init_y_translation(t_point *y, double value);
void	init_z_translation(t_point *z, double value);

void	mult_vec_matrix(const t_matrix *m, const t_point *p, t_point *t_p);
void	mult_matrix(const t_matrix *m1, const t_matrix *m2, t_matrix *m3);
#endif //MATRIX_H
