/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 15:29:04 by huthiess          #+#    #+#             */
/*   Updated: 2022/09/21 17:05:02 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSING_H
# define PARSING_H

# include "arch.h"
# include <stdbool.h>

int		parse_r(char *str, t_data *data);
int		parse_a(char *str, t_data *data);
int		parse_c(char *str, t_data *data);
int		parse_l(char *str, t_data *data);
int		parse_sp(char *str, t_data *data);
int		parse_pl(char *str, t_data *data);
int		parse_cy(char *str, t_data *data);

char	*init_var(double *val, char *str, char stop);
int		parse(char *filename, t_data *data);

bool	parse_uint8(char **str, u_int8_t *n, int min, int max);
bool	parse_double(char **str, double *d);
bool	parse_signed_double(char **str, double *d);
bool	parse_whitespaces(char **str);
bool	parse_char(char **str, char c);

char	*next_val(char *str, char stop);
char	*init_var(double *val, char *str, char stop);
int		check_range(double min, double max, double val);

#endif
