/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shape.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/14 19:44:55 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/14 19:44:55 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHAPE_H
# define SHAPE_H

# include "arch.h"

typedef struct s_tuple
{
	double	first;
	double	second;
}	t_tuple;

void	set_without_overflow(u_int8_t *dst, int new_val);
t_tuple	solve_quadratic(double a, double b, double c);

double	ray_sphere(void *sp, t_vec *ray);
void	colorize_sphere(t_data *data, t_ray *r_norm, double t, void *sp);

double	ray_plane(void *pl, t_vec *ray);
void	colorize_plane(t_data *data, t_ray *r_norm, double t, void *pl);

double	ray_cylinder(void *cy, t_vec *ray);
void	colorize_cylinder(t_data *data, t_ray *r_norm, double t, void *cy);
void	compute_rray(t_point *r0, t_point *v, double t, t_point *rray);

#endif //SHAPE_H
