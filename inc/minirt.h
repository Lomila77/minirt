/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minirt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 11:55:26 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/21 17:05:02 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINIRT_H
# define MINIRT_H

# include "arch.h"
# include "error.h"
# include "parsing.h"
# include "ray.h"
# include "utils.h"
# include "shape.h"
# include "matrix.h"
# include "../minilibx-linux/mlx.h"
# include "../libft/libft.h"

# include <stdbool.h>
# include <stdio.h>
# include <fcntl.h>

# define SCREEN_WIDTH 800
# define SCREEN_HEIGHT 800

/* Translation
 * R: Forward
 * F: Backward */
# define UP 114
# define LEFT 65361
# define DOWN 102
# define RIGHT 65363
# define R 65362
# define F 65364

/* Rotation */
# define W 119
# define S 115
# define A 97
# define D 100
# define Q 113
# define E 101

/* Quit object mode */
# define Z 122

/* Scale */
# define DIAM 116
# define _DIAM 121
# define HEI 103
# define _HEI 104

# define ESCAPE 65307

int	send_ray(t_data *data);

#endif //MINIRT_H
