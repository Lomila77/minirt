/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 17:52:40 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 17:52:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RAY_H
# define RAY_H

# include "arch.h"

void				compute_ray_direction(t_data *data, t_vec *vector,
						t_pxl *pxl);
void				normalize(t_ray *ray);
double				dot_product(t_point *a, t_point *b);
int					colors_to_mlx_colors(t_color *color);
void				mlx_colors_to_colors(t_mlx_color *mlx_color,
						t_color *color);
t_point				compute_hit_point(t_ray *ray, double t);
double				compute_diffuse(t_data *data, void *obj, t_point *hit_point,
						t_point *hit_normal);
void				colorize_obj(t_data *data, t_ray *ray, double diffuse,
						t_color *obj_color);
t_closest_object	closest_object(t_data *data, t_vec *r_norm, t_obj *ignore);

#endif //RAY_H
