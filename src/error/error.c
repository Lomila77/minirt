/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 15:57:00 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 17:12:20 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	e_bad_entry(char *str)
{
	printf("[-1] Bad entry in scene.rt file (%s).\n", str);
	return (-1);
}

int	e_open_file(char *str)
{
	printf("[-2] Problem openning the file (%s).\n", str);
	return (-2);
}

int	e_bad_read(char *str)
{
	printf("[-3] Problem reading the file (%s).\n", str);
	return (-3);
}

int	e_malloc_fail(char *str)
{
	printf("[-4] Malloc fail (%s)/\n", str);
	return (-4);
}

int	e_bad_arg(char *str)
{
	printf("[-5] Bad arguments (%s).\n", str);
	return (-5);
}
