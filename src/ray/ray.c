/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 17:52:16 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/16 19:17:32 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"
#include <math.h>

void	compute_ray_direction(t_data *data, t_vec *vector, t_pxl *pxl)
{
	double		ratio;
	double		scale;

	ratio = data->window.screen.width / data->window.screen.height;
	scale = tan(degres_to_radian(data->camera.fov * 0.5));
	vector->dir.x = (2 * (pxl->x + 0.5) / data->window.screen.width - 1)
		* ratio * scale;
	vector->dir.y = (1 - 2 * (pxl->y + 0.5) / data->window.screen.height)
		* scale;
	vector->dir.z = 1;
	normalize_point(&vector->dir);
	mult_vec_matrix(&data->camera.matrix, &vector->dir, &vector->dir);
	normalize_point(&vector->dir);
}

t_closest_object	ray_objects_type(t_vec *r_norm, t_obj *objects,
									t_obj *ignore)
{
	t_list				*current_object;
	double				t;
	t_closest_object	closest;

	closest.object = NULL;
	closest.t = INFINITY;
	closest.intersec = objects->intersec;
	closest.colorize_ray = objects->colorize_ray;
	current_object = objects->form;
	while (current_object != NULL)
	{
		if (current_object->content != ignore)
		{
			t = objects->intersec(current_object->content, r_norm);
			if (t >= 0 && t < closest.t)
			{
				closest.t = t;
				closest.object = current_object->content;
			}
		}
		current_object = current_object->next;
	}
	return (closest);
}

t_closest_object	closest_object(t_data *data, t_vec *r_norm, t_obj *ignore)
{
	t_closest_object	closest;
	t_closest_object	current;

	closest = ray_objects_type(r_norm, &data->array_form[SP], ignore);
	closest.type = SP;
	current = ray_objects_type(r_norm, &data->array_form[PL], ignore);
	if (current.t >= 0 && current.t < closest.t)
	{
		closest = current;
		closest.type = PL;
	}
	current = ray_objects_type(r_norm, &data->array_form[CY], ignore);
	if (current.t >= 0 && current.t < closest.t)
	{
		closest = current;
		closest.type = CY;
	}
	return (closest);
}

void	ray_all_objects(t_data *data, t_ray *r_norm)
{
	t_closest_object	closest;

	closest = closest_object(data, &r_norm->axe, NULL);
	if (closest.t >= 0 && closest.t != INFINITY)
		closest.colorize_ray(data, r_norm, closest.t, closest.object);
}

int	send_ray(t_data *data)
{
	t_ray	r_norm;
	t_pxl	pxl;

	r_norm.axe.ori = data->camera.axe.ori;
	pxl.y = 0;
	while (pxl.y < data->window.screen.height)
	{
		pxl.x = 0;
		while (pxl.x < data->window.screen.width)
		{
			r_norm.color = (t_color){.r = 0, .g = 0, .b = 0};
			compute_ray_direction(data, &r_norm.axe, &pxl);
			ray_all_objects(data, &r_norm);
			my_mlx_pixel_put(&data->mlx, pxl.x, pxl.y,
				colors_to_mlx_colors(&r_norm.color));
			pxl.x++;
		}
		pxl.y++;
	}
	mlx_put_image_to_window(data->mlx.id, data->window.id,
		data->mlx.img.id, 0, 0);
	return (0);
}
