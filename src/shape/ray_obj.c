/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_obj.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/28 14:48:57 by huthiess          #+#    #+#             */
/*   Updated: 2022/11/28 14:48:57 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"
#include <math.h>

t_point	compute_hit_point(t_ray *ray, double t)
{
	return ((t_point){
		.x = ray->axe.ori.x + ray->axe.dir.x * t,
		.y = ray->axe.ori.y + ray->axe.dir.y * t,
		.z = ray->axe.ori.z + ray->axe.dir.z * t});
}

#define OFFSET 0.00001

double	compute_diffuse(t_data *data, void *obj, t_point *hit_point,
						t_point *hit_normal)
{
	double				light_t;
	t_point				l;
	t_closest_object	closest;
	t_ray				light_ray;

	(void)obj;
	l = (t_point){
		.x = data->spot.ori.x - hit_point->x,
		.y = data->spot.ori.y - hit_point->y,
		.z = data->spot.ori.z - hit_point->z};
	light_t = norm(&l);
	normalize_point(&l);
	light_ray.axe = (t_vec){.dir = l};
	light_ray.axe.ori = (t_point){hit_point->x + hit_normal->x * OFFSET,
		hit_point->y + hit_normal->y * OFFSET,
		hit_point->z + hit_normal->z * OFFSET};
	closest = closest_object(data, &light_ray.axe, NULL);
	if (closest.t > 0 && closest.t < light_t)
		return (0);
	return (data->spot.light.ratio * fmax(0.f, dot_product(hit_normal, &l)));
}

void	colorize_obj(t_data *data, t_ray *ray, double diffuse,
					t_color *obj_color)
{
	ray->color.r = (data->ambient.ratio * data->ambient.color.r * obj_color->r
			+ diffuse * data->spot.light.color.r * obj_color->r);
	ray->color.g = (data->ambient.ratio * data->ambient.color.g * obj_color->g
			+ diffuse * data->spot.light.color.g * obj_color->g);
	ray->color.b = (data->ambient.ratio * data->ambient.color.b * obj_color->b
			+ diffuse * data->spot.light.color.b * obj_color->b);
}

void	compute_rray(t_point *r0, t_point *v, double t, t_point *rray)
{
	*rray = (t_point){r0->x + v->x * t, r0->y + v->y * t, r0->z + v->z * t};
}
