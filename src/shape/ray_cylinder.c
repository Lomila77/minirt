/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_cylinder.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 14:15:57 by huthiess          #+#    #+#             */
/*   Updated: 2022/12/02 14:15:57 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"
#include <math.h>

void	compute_cylinder_normal(t_ray *ray, t_cylinder *cylinder, double t,
	t_point *hit_normal)
{
	t_point	origin_intersec;
	t_point	rray;
	double	coef;
	t_point	center_at_inter;

	rray = (t_point){ray->axe.ori.x + ray->axe.dir.x * t,
		ray->axe.ori.y + ray->axe.dir.y * t,
		ray->axe.ori.z + ray->axe.dir.z * t};
	substract_point(&rray, &cylinder->axe.ori, &origin_intersec);
	coef = dot_product(&origin_intersec, &cylinder->axe.dir);
	center_at_inter = (t_point){
		cylinder->axe.ori.x + cylinder->axe.dir.x * coef,
		cylinder->axe.ori.y + cylinder->axe.dir.y * coef,
		cylinder->axe.ori.z + cylinder->axe.dir.z * coef};
	if (cylinder->inside)
		substract_point(&center_at_inter, &rray, hit_normal);
	else
		substract_point(&rray, &center_at_inter, hit_normal);
	normalize_point(hit_normal);
}

void	colorize_cylinder(t_data *data, t_ray *ray, double t, void *cy)
{
	t_cylinder	*cylinder;
	t_point		hit_point;
	t_point		hit_normal;
	double		diffuse;

	cylinder = cy;
	hit_point = compute_hit_point(ray, t);
	compute_cylinder_normal(ray, cylinder, t, &hit_normal);
	diffuse = compute_diffuse(data, cy, &hit_point,
			&hit_normal);
	colorize_obj(data, ray, diffuse, &cylinder->color);
}

bool	compute_cylinder_t(t_point *rray, t_point *ra1, t_point *ra2,
	t_point *s)
{
	t_point	rray_ra1;
	t_point	rray_ra2;

	substract_point(rray, ra1, &rray_ra1);
	substract_point(rray, ra2, &rray_ra2);
	return (dot_product(&rray_ra1, s) > 0 && dot_product(&rray_ra2, s) < 0);
}

double	compute_ray_cylinder_intersect(t_tuple t, t_vec *ray,
										t_cylinder *cylinder)
{
	t_point	ra2;
	t_point	rray;

	ra2 = (t_point){
		cylinder->axe.ori.x + cylinder->axe.dir.x * cylinder->height,
		cylinder->axe.ori.y + cylinder->axe.dir.y * cylinder->height,
		cylinder->axe.ori.z + cylinder->axe.dir.z * cylinder->height};
	if (t.first >= 0)
	{
		cylinder->inside = t.second < 0;
		compute_rray(&ray->ori, &ray->dir, t.first, &rray);
		if (compute_cylinder_t(&rray, &cylinder->axe.ori, &ra2,
				&cylinder->axe.dir))
			return (t.first);
		else
		{
			cylinder->inside = t.first > 0;
			compute_rray(&ray->ori, &ray->dir, t.second, &rray);
			if (compute_cylinder_t(&rray, &cylinder->axe.ori, &ra2,
					&cylinder->axe.dir))
				return (t.second);
		}
	}
	return (-1);
}

double	ray_cylinder(void *cy, t_vec *ray)
{
	t_cylinder	*cylinder;
	t_point		r0_ra1;
	t_point		ra0;
	t_point		va;

	cylinder = cy;
	r0_ra1 = (t_point){ray->ori.x - cylinder->axe.ori.x,
		ray->ori.y - cylinder->axe.ori.y,
		ray->ori.z - cylinder->axe.ori.z};
	cross(&cylinder->axe.dir, &r0_ra1, &ra0);
	cross(&ra0, &cylinder->axe.dir, &ra0);
	cross(&cylinder->axe.dir, &ray->dir, &va);
	cross(&va, &cylinder->axe.dir, &va);
	return (compute_ray_cylinder_intersect(
			solve_quadratic(dot_product(&va, &va),
				2 * dot_product(&ra0, &va),
				dot_product(&ra0, &ra0) - pow(cylinder->diameter / 2, 2)),
			ray, cylinder));
}
