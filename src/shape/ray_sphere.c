/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_sphere.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 11:51:55 by gcolomer          #+#    #+#             */
/*   Updated: 2021/03/11 11:51:55 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"
#include <math.h>
#include <limits.h>

void	set_without_overflow(u_int8_t *dst, int new_val)
{
	if (new_val < UCHAR_MAX)
		*dst = new_val;
	else
		*dst = UCHAR_MAX;
}

double	initialize_sphere_values(t_sphere *sphere, t_ray *r_norm,
									t_point *l, t_data *data)
{
	t_point		c;

	c = (t_point){
		.x = sphere->ori.x - r_norm->axe.ori.x,
		.y = sphere->ori.y - r_norm->axe.ori.y,
		.z = sphere->ori.z - r_norm->axe.ori.z};
	*l = (t_point){
		.x = sphere->ori.x - data->spot.ori.x,
		.y = sphere->ori.y - data->spot.ori.y,
		.z = sphere->ori.z - data->spot.ori.z};
	return (norm(&c));
}

void	initialize_hit_normal_sphere(t_point *hit_normal, t_point *hit_point,
										t_sphere *sphere)
{
	*hit_normal = (t_point){
		.x = hit_point->x - sphere->ori.x,
		.y = hit_point->y - sphere->ori.y,
		.z = hit_point->z - sphere->ori.z};
}

void	colorize_sphere(t_data *data, t_ray *r_norm, double t, void *sp)
{
	t_sphere	*sphere;
	t_point		hit_point;
	t_point		hit_normal;
	double		c_norm;
	t_point		l;

	sphere = sp;
	hit_point = compute_hit_point(r_norm, t);
	c_norm = initialize_sphere_values(sphere, r_norm, &l, data);
	initialize_hit_normal_sphere(&hit_normal, &hit_point, sphere);
	if (c_norm > sphere->diameter / 2)
	{
		if (norm(&l) < sphere->diameter / 2)
			hit_normal = l;
	}
	else
	{
		hit_normal = l;
		if (norm(&l) < sphere->diameter / 2)
			hit_normal = (t_point){-hit_normal.x, -hit_normal.y, -hit_normal.z};
	}
	normalize_point(&hit_normal);
	colorize_obj(data, r_norm, compute_diffuse(data, sp,
			&hit_point, &hit_normal), &sphere->color);
}

double	ray_sphere(void *sp, t_vec *ray)
{
	t_sphere	*s;
	t_point		l;
	double		b;
	double		c;

	s = sp;
	l = (t_point){
		.x = ray->ori.x - s->ori.x,
		.y = ray->ori.y - s->ori.y,
		.z = ray->ori.z - s->ori.z};
	b = 2 * dot_product(&ray->dir, &l);
	c = dot_product(&l, &l) - pow(s->diameter / 2, 2);
	return (solve_quadratic(1, b, c).first);
}
