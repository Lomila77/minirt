/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_plane.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 17:52:16 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/16 19:17:32 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"
#include <math.h>

void	colorize_plane(t_data *data, t_ray *r_norm, double t, void *pl)
{
	t_plane	*plane;
	t_point	hit_point;
	t_point	hit_normal;
	double	diffuse;

	plane = pl;
	hit_point = compute_hit_point(r_norm, t);
	hit_normal = plane->axe.dir;
	if (acos(dot_product(&r_norm->axe.dir, &plane->axe.dir)) < M_PI_2)
		hit_normal = (t_point){-hit_normal.x, -hit_normal.y, -hit_normal.y};
	diffuse = compute_diffuse(data, pl, &hit_point, &hit_normal);
	colorize_obj(data, r_norm, diffuse, &plane->color);
}

double	ray_plane(void *pl, t_vec *ray)
{
	double	denom;
	double	t;
	t_plane	*plane;
	t_point	p0l0;

	plane = (t_plane *)pl;
	denom = dot_product(&plane->axe.dir, &ray->dir);
	substract_point(&plane->axe.ori, &ray->ori, &p0l0);
	t = dot_product(&p0l0, &plane->axe.dir) / denom;
	return (t);
}
