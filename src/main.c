/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 11:49:51 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/21 17:14:31 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minirt.h"

void	set_tranformation(t_data *data)
{
	init_x_rotation(&data->transform.r_x, MOVE_R);
	init_x_rotation(&data->transform.rr_x, -MOVE_R);
	init_y_rotation(&data->transform.r_y, MOVE_R);
	init_y_rotation(&data->transform.rr_y, -MOVE_R);
	init_z_rotation(&data->transform.r_z, MOVE_R);
	init_z_rotation(&data->transform.rr_z, -MOVE_R);
	init_x_translation(&data->transform.t_x, MOVE_T);
	init_y_translation(&data->transform.t_y, MOVE_T);
	init_z_translation(&data->transform.t_z, MOVE_T);
	init_x_translation(&data->transform.rt_x, -MOVE_T);
	init_y_translation(&data->transform.rt_y, -MOVE_T);
	init_z_translation(&data->transform.rt_z, -MOVE_T);
}

void	set_data(t_data *data)
{
	int	i;

	i = 0;
	while (i < MAX_OBJ)
	{
		data->array_form[i].nb = 0;
		data->array_form[i].form = NULL;
		i++;
	}
	data->array_form[SP].intersec = &ray_sphere;
	data->array_form[SP].colorize_ray = &colorize_sphere;
	data->array_form[PL].intersec = &ray_plane;
	data->array_form[PL].colorize_ray = &colorize_plane;
	data->array_form[CY].intersec = &ray_cylinder;
	data->array_form[CY].colorize_ray = &colorize_cylinder;
	data->window.screen.width = SCREEN_WIDTH;
	data->window.screen.height = SCREEN_HEIGHT;
	data->obj_to_move = NULL;
	data->set_ambient = false;
	data->set_camera = false;
	data->set_spot = false;
	set_tranformation(data);
}

void	set_mlx(t_mlx *mlx, t_screen *screen)
{
	mlx->id = mlx_init();
	mlx->img.id = mlx_new_image(mlx->id, screen->width, screen->height);
	mlx->img.addr = mlx_get_data_addr(mlx->img.id, &mlx->img.bits_per_pixel,
			&mlx->img.line_length, &mlx->img.endian);
}

int	main(int argc, char **argv)
{
	t_data	data;

	if (argc != 2)
		return (e_bad_arg("Need .rt scene file"));
	set_data(&data);
	if (parse(argv[1], &data) != 0)
		return (-1);
	set_mlx(&data.mlx, &data.window.screen);
	new_window(&data, &data.window);
	if (data.window.id == NULL)
		return (quit(&data));
	mlx_expose_hook(data.window.id, send_ray, &data);
	mlx_key_hook(data.window.id, &key, &data);
	mlx_mouse_hook(data.window.id, &mouse, &data);
	mlx_hook(data.window.id, 17, 0L, &quit, &data);
	mlx_put_image_to_window(data.mlx.id, data.window.id, data.mlx.img.id, 0, 0);
	mlx_loop(data.mlx.id);
	return (0);
}
