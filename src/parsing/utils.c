/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 03:54:52 by huthiess          #+#    #+#             */
/*   Updated: 2022/10/27 03:54:52 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"
#include <math.h>

bool	parse_uint8(char **str, u_int8_t *n, int min, int max)
{
	int	tmp;

	tmp = 0;
	if (!ft_isdigit(**str))
		return (false);
	while (ft_isdigit(**str))
	{
		tmp = tmp * 10 + **str - '0';
		(*str)++;
	}
	*n = tmp;
	return (min <= tmp && tmp <= max);
}

bool	parse_double(char **str, double *d)
{
	int	exp;

	*d = 0;
	exp = 0;
	if (!ft_isdigit(**str))
		return (false);
	while (ft_isdigit(**str))
	{
		*d = *d * 10 + **str - '0';
		(*str)++;
	}
	if (**str == '.')
	{
		(*str)++;
		if (!ft_isdigit(**str))
			return (false);
		while (ft_isdigit(**str))
		{
			*d = *d * 10 + (**str - '0');
			exp++;
			(*str)++;
		}
		*d *= pow(10, -exp);
	}
	return (true);
}

bool	parse_signed_double(char **str, double *d)
{
	int		sign;

	sign = 1;
	if ((**str) == '-')
	{
		sign = -1;
		(*str)++;
	}
	else if ((**str) == '+')
		(*str)++;
	if (parse_double(str, d) == false)
		return (false);
	*d *= sign;
	return (true);
}

bool	parse_whitespaces(char **str)
{
	if (**str != ' ')
		return (false);
	while (**str == ' ')
		(*str)++;
	return (true);
}

bool	parse_char(char **str, char c)
{
	if (**str != c)
		return (false);
	(*str)++;
	return (true);
}
