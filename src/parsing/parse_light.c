/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_light.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/15 16:18:00 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 15:53:52 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	parse_l(char *str, t_data *data)
{
	t_mlx_color	color;

	str += 1;
	if (parse_whitespaces(&str)
		&& parse_signed_double(&str, &data->spot.ori.x)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &data->spot.ori.y)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &data->spot.ori.z)
		&& parse_whitespaces(&str)
		&& parse_signed_double(&str, &data->spot.light.ratio)
		&& data->spot.light.ratio >= 0 && data->spot.light.ratio <= 1
		&& parse_whitespaces(&str)
		&& parse_uint8(&str, &color.c.r, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color.c.g, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color.c.b, 0, 255)
		&& parse_char(&str, '\0'))
	{
		mlx_colors_to_colors(&color, &data->spot.light.color);
		data->set_spot = true;
		return (0);
	}
	return (e_bad_entry("ambient's value"));
}
