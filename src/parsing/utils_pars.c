/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_pars.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/31 14:56:10 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 15:53:43 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

char	*next_val(char *str, char stop)
{
	int	p;

	p = 0;
	while (ft_space(*str) == 1)
		str++;
	if (*str == '-')
		str++;
	while ((ft_isnum((*str)) || (((*str) == '.') && p < 1)) && (*str))
	{
		if ((*str) == '.')
			p = 1;
		str++;
	}
	if (*str == stop)
		str++;
	else
		str = NULL;
	return (str);
}

char	*init_var(double *val, char *str, char stop)
{
	if (str != NULL)
	{
		*val = ft_atod(str);
		str = next_val(str, stop);
	}
	return (str);
}

int	check_range(double min, double max, double val)
{
	if (val < min || val > max)
		return (-1);
	return (0);
}
