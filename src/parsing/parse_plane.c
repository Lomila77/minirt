/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_plane.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/30 11:56:31 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 15:48:51 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	check_pl_parse(char *str, t_plane *pl, t_mlx_color *color)
{
	if (parse_whitespaces(&str)
		&& parse_signed_double(&str, &pl->axe.ori.x)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &pl->axe.ori.y)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &pl->axe.ori.z)
		&& parse_whitespaces(&str)
		&& parse_signed_double(&str, &pl->axe.dir.x)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &pl->axe.dir.y)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &pl->axe.dir.z)
		&& parse_whitespaces(&str)
		&& parse_uint8(&str, &color->c.r, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color->c.g, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color->c.b, 0, 255)
		&& parse_char(&str, '\0'))
		return (0);
	return (-1);
}

int	parse_pl(char *str, t_data *data)
{
	t_plane		*pl;
	t_mlx_color	color;

	str += 2;
	pl = malloc(sizeof(t_plane));
	if (pl == NULL)
		return (e_malloc_fail("plane alloc"));
	if (check_pl_parse(str, pl, &color) != -1)
	{
		ft_lstadd_front(&(data->array_form[PL].form), ft_lstnew(pl));
		mlx_colors_to_colors(&color, &pl->color);
		normalize_point(&pl->axe.dir);
		data->array_form[PL].nb++;
		set_obj_matrix(&pl->matrix, &pl->axe.ori, &pl->axe.dir);
		return (0);
	}
	return (e_bad_entry("plane's value"));
}
