/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_resolution.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/15 13:48:23 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 15:52:08 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	check_res(t_window *w)
{
	if (w->screen.width == 0 || w->screen.height == 0)
		return (-1);
	return (0);
}

void	resolution_is_false(t_window *w, char *str)
{
	if (str == NULL)
	{
		w->screen.width = 0;
		w->screen.height = 0;
	}
}

int	parse_r(char *str, t_data *data)
{
	str += 1;
	data->window.screen.width = ft_atod(str);
	str = next_val(str, ' ');
	resolution_is_false(&data->window, str);
	data->window.screen.height = ft_atod(str);
	if (check_res(&data->window))
		return (e_bad_entry("resolution's value"));
	return (1);
}
