/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_sphere.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/30 11:56:31 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 15:48:51 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	check_sp_parse(char *str, t_sphere *sp, t_mlx_color *color)
{
	if (parse_whitespaces(&str)
		&& parse_signed_double(&str, &sp->ori.x)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &sp->ori.y)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &sp->ori.z)
		&& parse_whitespaces(&str)
		&& parse_signed_double(&str, &sp->diameter)
		&& parse_whitespaces(&str)
		&& parse_uint8(&str, &color->c.r, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color->c.g, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color->c.b, 0, 255)
		&& parse_char(&str, '\0'))
		return (0);
	return (-1);
}

int	parse_sp(char *str, t_data *data)
{
	t_sphere	*sp;
	t_mlx_color	color;

	str += 2;
	sp = malloc(sizeof(t_sphere));
	if (sp == NULL)
		return (e_malloc_fail("sphere alloc"));
	if (check_sp_parse(str, sp, &color) != -1)
	{
		ft_lstadd_front(&(data->array_form[SP].form), ft_lstnew(sp));
		mlx_colors_to_colors(&color, &sp->color);
		data->array_form[SP].nb++;
		set_obj_matrix(&sp->matrix, &sp->ori, NULL);
		return (0);
	}
	return (e_bad_entry("sphere's value"));
}
