/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 14:40:14 by huthiess          #+#    #+#             */
/*   Updated: 2022/09/21 17:08:56 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	open_file(int *fd, char *scene)
{
	int	len;

	len = ft_strlen(scene);
	if (scene[len - 1] != 't' || scene[len - 2] != 'r' || scene[len - 3] != '.')
		return (e_bad_entry("Bad scene format"));
	*fd = open(scene, O_RDONLY);
	if (*fd < 0)
		return (e_open_file("gnl"));
	return (0);
}

int	parse_line(char *line, t_data *data)
{
	int	res;

	if (line[0] == '\0')
		res = 0;
	else if (ft_strncmp(line, "A", 1) == 0 && data->set_ambient == false)
		res = parse_a(line, data);
	else if (ft_strncmp(line, "C", 1) == 0 && data->set_camera == false)
		res = parse_c(line, data);
	else if (ft_strncmp(line, "L", 1) == 0 && data->set_spot == false)
		res = parse_l(line, data);
	else if (ft_strncmp(line, "sp", 2) == 0)
		res = parse_sp(line, data);
	else if (ft_strncmp(line, "pl", 2) == 0)
		res = parse_pl(line, data);
	else if (ft_strncmp(line, "cy", 2) == 0)
		res = parse_cy(line, data);
	else
		res = e_bad_entry("identifer");
	free(line);
	return (res);
}

int	parse(char *filename, t_data *data)
{
	int		fd;
	char	*line;

	line = NULL;
	if (open_file(&fd, filename) != 0)
		return (-1);
	while (get_next_line(fd, &line) == 1)
	{
		if (parse_line(line, data) != 0)
			return (-1);
		line = NULL;
	}
	if (line != NULL && parse_line(line, data) != 0)
		return (-1);
	return (0);
}
