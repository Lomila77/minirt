/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cylinder.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/02 14:19:31 by huthiess          #+#    #+#             */
/*   Updated: 2022/12/02 14:19:51 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	check_cy_parse(char *str, t_cylinder *cy, t_mlx_color *color)
{
	if (parse_whitespaces(&str)
		&& parse_signed_double(&str, &cy->axe.ori.x)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &cy->axe.ori.y)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &cy->axe.ori.z)
		&& parse_whitespaces(&str)
		&& parse_signed_double(&str, &cy->axe.dir.x)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &cy->axe.dir.y)
		&& parse_char(&str, ',')
		&& parse_signed_double(&str, &cy->axe.dir.z)
		&& parse_whitespaces(&str)
		&& parse_signed_double(&str, &cy->diameter)
		&& parse_whitespaces(&str)
		&& parse_signed_double(&str, &cy->height)
		&& parse_whitespaces(&str)
		&& parse_uint8(&str, &color->c.r, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color->c.g, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color->c.b, 0, 255)
		&& parse_char(&str, '\0'))
		return (0);
	return (-1);
}

int	parse_cy(char *str, t_data *data)
{
	t_cylinder	*cy;
	t_mlx_color	color;

	str += 2;
	cy = malloc(sizeof(t_cylinder));
	if (cy == NULL)
		return (e_malloc_fail("plane alloc"));
	if (check_cy_parse(str, cy, &color) != -1)
	{
		normalize_point(&cy->axe.dir);
		ft_lstadd_front(&(data->array_form[CY].form), ft_lstnew(cy));
		mlx_colors_to_colors(&color, &cy->color);
		set_obj_matrix(&cy->matrix, &cy->axe.ori, &cy->axe.dir);
		data->array_form[CY].nb++;
		return (0);
	}
	return (e_bad_entry("cylinder's value"));
}
