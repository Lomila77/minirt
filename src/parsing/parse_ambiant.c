/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_ambiant.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/15 15:13:45 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/01 15:52:54 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	parse_a(char *str, t_data *data)
{
	t_mlx_color	color;

	str += 1;
	if (parse_whitespaces(&str)
		&& parse_signed_double(&str, &data->ambient.ratio)
		&& data->ambient.ratio >= 0 && data->ambient.ratio <= 1
		&& parse_whitespaces(&str)
		&& parse_uint8(&str, &color.c.r, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color.c.g, 0, 255)
		&& parse_char(&str, ',')
		&& parse_uint8(&str, &color.c.b, 0, 255)
		&& parse_char(&str, '\0'))
	{
		data->set_ambient = true;
		mlx_colors_to_colors(&color, &data->ambient.color);
		return (0);
	}
	return (e_bad_entry("ambient's value"));
}
