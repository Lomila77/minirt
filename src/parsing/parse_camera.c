/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_camera.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 15:52:39 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 15:20:51 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

bool	control_cam(t_cam *cam)
{
	if (!control_val(0, 180, cam->fov)
		|| !control_val(-1, 1, cam->axe.dir.x)
		|| !control_val(-1, 1, cam->axe.dir.y)
		|| !control_val(-1, 1, cam->axe.dir.z))
		return (false);
	return (true);
}

bool	init_cam(t_cam *cam, char **str)
{
	if (parse_whitespaces(str)
		&& parse_signed_double(str, &cam->axe.ori.x)
		&& parse_char(str, ',')
		&& parse_signed_double(str, &cam->axe.ori.y)
		&& parse_char(str, ',')
		&& parse_signed_double(str, &cam->axe.ori.z)
		&& parse_whitespaces(str)
		&& parse_signed_double(str, &cam->axe.dir.x)
		&& parse_char(str, ',')
		&& parse_signed_double(str, &cam->axe.dir.y)
		&& parse_char(str, ',')
		&& parse_signed_double(str, &cam->axe.dir.z)
		&& parse_whitespaces(str)
		&& parse_signed_double(str, &cam->fov)
		&& parse_char(str, '\0'))
		return (control_cam(cam));
	return (false);
}

int	parse_c(char *str, t_data *data)
{
	t_cam	cam;

	str++;
	if (!init_cam(&cam, &str))
		return (e_bad_entry("camera's value"));
	normalize_point(&cam.axe.dir);
	set_obj_matrix(&cam.matrix, &cam.axe.ori, &cam.axe.dir);
	data->camera = cam;
	data->set_camera = true;
	return (0);
}
