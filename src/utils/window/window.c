/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 15:52:39 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 15:20:51 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/minirt.h"

void	my_mlx_pixel_put(t_mlx *mlx, int x, int y, int color)
{
	char	*dst;

	dst = mlx->img.addr + (y * mlx->img.line_length + x
			* (mlx->img.bits_per_pixel / 8));
	*(unsigned int *)dst = color;
}

/* TODO change "miniRT" to data->name */
void	new_window(t_data *data, t_window *win)
{
	win->id = mlx_new_window(data->mlx.id, win->screen.width,
			win->screen.height, "miniRT");
	if (win->id == NULL)
	{
		ft_putstr_fd("Erreur lors de la creation de la fenetre", 2);
		ft_putstr_fd("C'est rate !", 1);
	}
}

int	quit(t_data *data)
{
	mlx_destroy_image(data->mlx.id, data->mlx.img.id);
	mlx_destroy_window(data->mlx.id, data->window.id);
	mlx_destroy_display(data->mlx.id);
	free(data->mlx.id);
	free_obj(data->array_form);
	exit (0);
}
