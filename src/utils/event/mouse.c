/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 15:52:39 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 15:20:51 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/minirt.h"

int	mouse(int button, int x, int y, t_data *data)
{
	t_closest_object	closest;
	t_vec				vector;
	t_pxl				pxl;

	(void)button;
	pxl.x = x;
	pxl.y = y;
	vector.ori.x = data->camera.axe.ori.x;
	vector.ori.y = data->camera.axe.ori.y;
	vector.ori.z = data->camera.axe.ori.z;
	compute_ray_direction(data, &vector, &pxl);
	closest = closest_object(data, &vector, NULL);
	data->obj_to_move = closest.object;
	data->type_obj_to_move = closest.type;
	mlx_key_hook(data->window.id, &key, data);
	return (0);
}
