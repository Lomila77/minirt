/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 15:52:39 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 15:20:51 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/minirt.h"

void	translate(t_matrix *m, t_point *t_type, t_point *res)
{
	t_point	tmp;

	mult_vec_matrix(m, t_type, &tmp);
	if (tmp.x == 0 && tmp.y == 0 && tmp.z == 0)
		tmp = *t_type;
	add_point(&tmp, res, res);
	m->c[3][0] = res->x;
	m->c[3][1] = res->y;
	m->c[3][2] = res->z;
}

void	tr_transform(int keycode, t_matrix *obj,
					t_transform *transform, t_point *p)
{
	if (keycode == UP)
		translate(obj, &transform->t_y, p);
	else if (keycode == DOWN)
		translate(obj, &transform->rt_y, p);
	else if (keycode == RIGHT)
		translate(obj, &transform->t_x, p);
	else if (keycode == LEFT)
		translate(obj, &transform->rt_x, p);
	else if (keycode == R)
		translate(obj, &transform->t_z, p);
	else if (keycode == F)
		translate(obj, &transform->rt_z, p);
}

void	ro_transform(int keycode, t_matrix *obj,
					t_transform *transform, t_matrix *res)
{
	if (keycode == W)
		mult_matrix(obj, &transform->rr_x, res);
	else if (keycode == S)
		mult_matrix(obj, &transform->r_x, res);
	else if (keycode == A)
		mult_matrix(obj, &transform->rr_y, res);
	else if (keycode == D)
		mult_matrix(obj, &transform->r_y, res);
	else if (keycode == Q)
		mult_matrix(obj, &transform->rr_z, res);
	else if (keycode == E)
		mult_matrix(obj, &transform->r_z, res);
}

void	sc_transform(int key, t_data *data)
{
	t_sphere	*sp;
	t_cylinder	*cy;

	if (data->obj_to_move == NULL)
		return ;
	else if (data->type_obj_to_move == SP)
	{
		sp = (t_sphere *)data->obj_to_move;
		if (key == DIAM)
			sp->diameter += MOVE_D;
		else if (key == _DIAM && (sp->diameter - MOVE_D) > 0)
			sp->diameter -= MOVE_D;
	}
	else if (data->type_obj_to_move == CY)
	{
		cy = (t_cylinder *)data->obj_to_move;
		if (key == DIAM)
			cy->diameter += MOVE_D;
		else if (key == _DIAM && cy->diameter - MOVE_D > 0)
			cy->diameter -= MOVE_D;
		else if (key == HEI)
			cy->height += MOVE_H;
		else if (key == _HEI && cy->height - MOVE_H > 0)
			cy->height -= MOVE_H;
	}
}

int	key(int key, t_data *data)
{
	t_matrix	*obj;
	t_point		*ori;
	t_point		*dir;

	dir = NULL;
	if (key == Z)
		data->obj_to_move = NULL;
	set_type(data, &obj, &ori, &dir);
	if (key == ESCAPE)
		return (quit(data));
	else if (key == DIAM || key == HEI || key == _DIAM || key == _HEI)
		sc_transform(key, data);
	else if (key != Z && (key == UP || key == DOWN || key == RIGHT
			|| key == LEFT || key == R || key == F))
		tr_transform(key, &data->camera.matrix, &data->transform, ori);
	else if (key != Z && dir != NULL && (key == W || key == S
			|| key == A || key == D || key == Q || key == E))
	{
		ro_transform(key, &data->camera.matrix, &data->transform, obj);
		if (data->type_obj_to_move == PL || data->type_obj_to_move == CY)
			mult_vec_matrix(obj, dir, dir);
		normalize_point(dir);
	}
	send_ray(data);
	return (0);
}
