/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   translation.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 16:00:08 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 11:27:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/minirt.h"

void	init_x_translation(t_point *x, double value)
{
	x->x = value;
	x->y = 0;
	x->z = 0;
}

void	init_y_translation(t_point *y, double value)
{
	y->x = 0;
	y->y = value;
	y->z = 0;
}

void	init_z_translation(t_point *z, double value)
{
	z->x = 0;
	z->y = 0;
	z->z = value;
}
