/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 16:00:08 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 11:27:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/minirt.h"
#include <math.h>

void	init_x_rotation(t_matrix *x, double val)
{
	double	value;
	double	cosinus;
	double	sinus;

	value = degres_to_radian(val);
	cosinus = cos(value);
	sinus = sin(value);
	x->c[0][0] = 1;
	x->c[0][1] = 0;
	x->c[0][2] = 0;
	x->c[0][3] = 0;
	x->c[1][0] = 0;
	x->c[1][1] = cosinus;
	x->c[1][2] = -sinus;
	x->c[1][3] = 0;
	x->c[2][0] = 0;
	x->c[2][1] = sinus;
	x->c[2][2] = cosinus;
	x->c[2][3] = 0;
	x->c[3][0] = 0;
	x->c[3][1] = 0;
	x->c[3][2] = 0;
	x->c[3][3] = 1;
}

void	init_y_rotation(t_matrix *y, double val)
{
	double	value;
	double	cosinus;
	double	sinus;

	value = degres_to_radian(val);
	cosinus = cos(value);
	sinus = sin(value);
	y->c[0][0] = cosinus;
	y->c[0][1] = 0;
	y->c[0][2] = sinus;
	y->c[0][3] = 0;
	y->c[1][0] = 0;
	y->c[1][1] = 1;
	y->c[1][2] = 0;
	y->c[1][3] = 0;
	y->c[2][0] = -sinus;
	y->c[2][1] = 0;
	y->c[2][2] = cosinus;
	y->c[2][3] = 0;
	y->c[3][0] = 0;
	y->c[3][1] = 0;
	y->c[3][2] = 0;
	y->c[3][3] = 1;
}

void	init_z_rotation(t_matrix *z, double val)
{
	double	value;
	double	cosinus;
	double	sinus;

	value = degres_to_radian(val);
	cosinus = cos(value);
	sinus = sin(value);
	z->c[0][0] = cosinus;
	z->c[0][1] = -sinus;
	z->c[0][2] = 0;
	z->c[0][3] = 0;
	z->c[1][0] = sinus;
	z->c[1][1] = cosinus;
	z->c[1][2] = 0;
	z->c[1][3] = 0;
	z->c[2][0] = 0;
	z->c[2][1] = 0;
	z->c[2][2] = 1;
	z->c[2][3] = 0;
	z->c[3][0] = 0;
	z->c[3][1] = 0;
	z->c[3][2] = 0;
	z->c[3][3] = 1;
}
