/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_matrix.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 16:00:08 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 11:27:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/minirt.h"

void	set_obj_matrix(t_matrix *matrix, t_point *ori, t_point *dir)
{
	t_point	x;
	t_point	y;
	t_point	z;

	matrix->c[3][0] = ori->x;
	matrix->c[3][1] = ori->y;
	matrix->c[3][2] = ori->z;
	matrix->c[3][3] = 1;
	if (dir == NULL)
		return ;
	z = *dir;
	cross(&(t_point){0, 1, 0}, &z, &x);
	cross(&z, &x, &y);
	matrix->c[0][0] = x.x;
	matrix->c[0][1] = x.y;
	matrix->c[0][2] = x.z;
	matrix->c[0][3] = 0;
	matrix->c[1][0] = y.x;
	matrix->c[1][1] = y.y;
	matrix->c[1][2] = y.z;
	matrix->c[1][3] = 0;
	matrix->c[2][0] = z.x;
	matrix->c[2][1] = z.y;
	matrix->c[2][2] = z.z;
	matrix->c[2][3] = 0;
}
