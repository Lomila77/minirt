/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/01 16:00:08 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/10 11:27:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/minirt.h"

void	mult_vec_matrix(const t_matrix *m, const t_point *p, t_point *t_p)
{
	t_point	tmp;

	tmp.x = p->x * m->c[0][0] + p->y * m->c[0][1] + p->z * m->c[0][2];
	tmp.y = p->x * m->c[1][0] + p->y * m->c[1][1] + p->z * m->c[1][2];
	tmp.z = p->x * m->c[2][0] + p->y * m->c[2][1] + p->z * m->c[2][2];
	t_p->x = tmp.x;
	t_p->y = tmp.y;
	t_p->z = tmp.z;
}

double	mult_line(const t_matrix *m1, const t_matrix *m2, int line, int colone)
{
	return (m1->c[line][0] * m2->c[0][colone]
		+ m1->c[line][1] * m2->c[1][colone]
		+ m1->c[line][2] * m2->c[2][colone]
		+ m1->c[line][3] * m2->c[3][colone]);
}

void	mult_matrix(const t_matrix *m1, const t_matrix *m2, t_matrix *m3)
{
	t_matrix	res;
	int			i;
	int			j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			res.c[i][j] = mult_line(m1, m2, i, j);
			j++;
		}
		i++;
	}
	*m3 = res;
}
