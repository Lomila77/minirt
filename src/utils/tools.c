/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/10 15:21:12 by gcolomer          #+#    #+#             */
/*   Updated: 2022/09/14 19:38:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"
#include <math.h>

bool	control_val(double min, double max, double val)
{
	if (val < min || val > max)
		return (false);
	return (true);
}

double	degres_to_radian(double degres)
{
	return (degres * (M_PI / 180));
}

void	set_type(t_data *data, t_matrix **obj, t_point **ori,
			t_point **dir)
{
	*obj = &data->camera.matrix;
	*ori = &data->camera.axe.ori;
	*dir = &data->camera.axe.dir;
	if (data->obj_to_move != NULL)
	{
		if (data->type_obj_to_move == SP)
		{
			*obj = &((t_sphere *)data->obj_to_move)->matrix;
			*ori = &((t_sphere *)data->obj_to_move)->ori;
		}
		else if (data->type_obj_to_move == PL)
		{
			*obj = &((t_plane *)data->obj_to_move)->matrix;
			*ori = &((t_plane *)data->obj_to_move)->axe.ori;
			*dir = &((t_plane *)data->obj_to_move)->axe.dir;
		}
		else if (data->type_obj_to_move == CY)
		{
			*obj = &((t_cylinder *)data->obj_to_move)->matrix;
			*ori = &((t_cylinder *)data->obj_to_move)->axe.ori;
			*dir = &((t_cylinder *)data->obj_to_move)->axe.dir;
		}
	}
}

void	cross(t_point *src1, t_point *src2, t_point *dst)
{
	t_point	tmp;

	tmp.x = src1->y * src2->z - src1->z * src2->y;
	tmp.y = src1->z * src2->x - src1->x * src2->z;
	tmp.z = src1->x * src2->y - src1->y * src2->x;
	*dst = tmp;
}

#define MINUS_ONE -1

t_tuple	solve_quadratic(double a, double b, double c)
{
	double	delta;
	double	q;
	double	t0;
	double	t1;

	delta = b * b - 4 * a * c;
	if (delta < 0)
		return ((t_tuple){
			MINUS_ONE,
			MINUS_ONE});
	else if (delta == 0)
		return ((t_tuple){-0.5 * b / a, -0.5 * b / a});
	else
	{
		if (b > 0)
			q = -0.5 * (b + sqrt(delta));
		else
			q = -0.5 * (b - sqrt(delta));
		t0 = q / a;
		t1 = c / q;
		if (t0 >= 0 && (t1 <= 0 || t0 < t1))
			return ((t_tuple){t0, t1});
		else
			return ((t_tuple){t1, t0});
	}
}
