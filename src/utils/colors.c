/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/25 17:48:36 by huthiess          #+#    #+#             */
/*   Updated: 2022/11/25 17:48:36 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/minirt.h"

int	colors_to_mlx_colors(t_color *color)
{
	t_mlx_color	mlx_color;

	set_without_overflow(&mlx_color.c.r, color->r * 255);
	set_without_overflow(&mlx_color.c.g, color->g * 255);
	set_without_overflow(&mlx_color.c.b, color->b * 255);
	return (mlx_color.mlx_color);
}

void	mlx_colors_to_colors(t_mlx_color *mlx_color, t_color *color)
{
	color->r = mlx_color->c.r / 255.0;
	color->g = mlx_color->c.g / 255.0;
	color->b = mlx_color->c.b / 255.0;
}
