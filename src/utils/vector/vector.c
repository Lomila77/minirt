/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: huthiess <huthiess@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/15 12:42:04 by huthiess          #+#    #+#             */
/*   Updated: 2022/11/15 12:42:04 by huthiess         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/minirt.h"
#include <math.h>

double	norm(t_point *p)
{
	return (sqrt(p->x * p->x + p->y * p->y + p->z * p->z));
}

void	normalize_point(t_point *point)
{
	double	n;

	n = norm(point);
	point->x /= n;
	point->y /= n;
	point->z /= n;
}

double	dot_product(t_point *a, t_point *b)
{
	return (a->x * b->x + a->y * b->y + a->z * b->z);
}

void	substract_point(t_point *a, t_point *b, t_point *res)
{
	t_point	tmp;

	tmp.x = a->x - b->x;
	tmp.y = a->y - b->y;
	tmp.z = a->z - b->z;
	res->x = tmp.x;
	res->y = tmp.y;
	res->z = tmp.z;
}

void	add_point(t_point *a, t_point *b, t_point *dst)
{
	t_point	tmp;

	tmp.x = a->x + b->x;
	tmp.y = a->y + b->y;
	tmp.z = a->z + b->z;
	dst->x = tmp.x;
	dst->y = tmp.y;
	dst->z = tmp.z;
}
